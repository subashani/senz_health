package com.score.senz_health.interfaces;

public interface IFragmentTransitionListener {
    void onTransition(String type);
}
