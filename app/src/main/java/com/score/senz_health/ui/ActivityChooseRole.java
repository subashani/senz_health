package com.score.senz_health.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.score.senz_health.R;

public class ActivityChooseRole extends AppCompatActivity {

    Button patientBtn;
    Button doctorBtn;
    Button laboratoryBtn;

    private static final String TAG = "SharedPreference"; //Tag for Log
    private static final String TAG_PA = "PhoneAuthActivity";
    private static final String G_TAG = "Global";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_role);
        Log.d(TAG_PA, "Entered Choose Role");
        patientBtn = (Button) findViewById(R.id.patient_button);
        doctorBtn = (Button) findViewById(R.id.doctor_button);
        laboratoryBtn = (Button) findViewById(R.id.laboratory_button);

        patientBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Making an indicator to assign the role permanently should be here
                //Using Shared preferences for this

                SharedPreferences sharedPreferences = getSharedPreferences("MyRole", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Role", "Patient");
                editor.commit();

                Log.d(G_TAG, "Saved Role Patient"); //Displayed in the android Montitor.

                Intent intent = new Intent(ActivityChooseRole.this, DrawerActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK); // clears all previous activities task
                intent.putExtra("role","Patient");
                finish();  // destroy current activity..
                startActivity(intent);
            }
        });

        doctorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Making an indicator to assign the role permanently should be here
                //Using Shared preferences for this

                SharedPreferences sharedPreferences = getSharedPreferences("MyRole", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Role", "Doctor");
                editor.commit();

                Log.d(G_TAG, "Saved Role Doctor"); //Displayed in the android Montitor.

                Intent intent = new Intent(ActivityChooseRole.this, DrawerActivity.class);
                intent.putExtra("role","Doctor");
                startActivity(intent);
            }
        });

        laboratoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Making an indicator to assign the role permanently should be here
                //Using Shared preferences for this

                SharedPreferences sharedPreferences = getSharedPreferences("MyRole", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Role", "Laboratory");
                editor.commit();

                Log.d(G_TAG, "Saved Role Laboratory"); //Displayed in the android Montitor.

                Intent intent = new Intent(ActivityChooseRole.this, DrawerActivity.class);
                intent.putExtra("role","Laboratory");
                startActivity(intent);
            }
        });


    }




}
